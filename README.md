# DevOps Take-Home Exercise

## Description

   * Base on the preamble of the project lets assume that my cluster is running on AWS, specifically in EKS, which is a managed kubernetes service. I’m also assuming that we have worker nodes on private subnets and that I have a load balancer controller already setup.

## Details

Taking in consideration the information provided I created a deployment with the following characteristics:


- [ ] Two applications running independently from each other on private subnets and automatically scaling when CPU utilization goes over 70%.
- [ ] Both applications can access the same database deployed using Amazon RDS, the can access the database using and ExternalName service which has the endpoint of the database as a parameter.
- [ ] Since both applications live on private subnets I decided to use an AWS Application Load Balancer to forward traffic to each app depending the path request from the users, if they want to access the users on the database the will add /users and the end of the load balancer dns name and if they want to access shifts information they will add the /shifts.
- [ ] Both apps are set to use rolling deployments using a rolling strategy and rollbacks can be accomplish since we are storing the latest replicasets that the app has used. I n other words, keeping the previous ReplicaSets around is a convenient mechanism to roll back to a previously working version of your app.
- [ ] There is also a cluster role created that can be bind to other users so they can perform admin tasks on the cluster. The role name is superuser.

## Additional information

Now if the deployment was to be use in a multi environment situation we can use namespaces to isolate each environment. That part can be done by adding the namespace parameter to our deployments.